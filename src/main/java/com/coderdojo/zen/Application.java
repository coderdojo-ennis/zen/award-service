package com.coderdojo.zen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Javadoc
 */
@SpringBootApplication
public class Application {

	/**
	 * Javadoc
	 * @param args Example
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Sole constructor. (For invocation by subclass
	 * constructors, typically implicit.)
	 */
	public Application() { /* Default Constructor */ }

}
